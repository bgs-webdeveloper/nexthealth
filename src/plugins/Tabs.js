import { tns } from 'tiny-slider/src/tiny-slider.js';

export class Slider {
  constructor() {
    this.thumbnails = tns({
      container: '.js-Tabs',
      items: 3,
      mode: 'carousel',
      mouseDrag: true,
      autoWidth: true,
      lazyload: false,
      prevButton: '.js-Tabs__arrow--left',
      nextButton: '.js-Tabs__arrow--right',
      loop: false,
      slideBy: 1,
      nav: false,
      center: true,
      gutter: 15,
      responsive: {
        1401: {
          center: false,
          gutter: 0
        }
      },
      preventScrollOnTouch: 'auto'
    });

    this.slider = tns({
      container: '.js-Tabs__slider',
      items: 1,
      mode: 'carousel',
      mouseDrag: true,
      lazyload: false,
      prevButton: '.js-Tabs__arrow--left',
      nextButton: '.js-Tabs__arrow--right',
      slideBy: 'page',
      loop: false,
      navContainer: '.js-Tabs',
      preventScrollOnTouch: 'auto'
    });

    this.thumbnails.events.on( 'indexChanged', evt => this.nextThumbnail( evt ));
    this.slider.events.on( 'indexChanged', evt => this.nextSlide( evt ));
  }

  nextSlide( evt ) {
    // hack beacause of strange idexes from tns slider
    const { slideCount } = this.slider.getInfo();
    const { index } = this.thumbnails.getInfo();
    const indexToGoTo = evt.index > slideCount ? 0 : evt.index;

    setTimeout(() => {
      if (indexToGoTo !== index) {
        this.thumbnails.goTo( indexToGoTo );
      }
    });
  }

  nextThumbnail( evt ) {
    // hack beacause of strange idexes from tns slider
    const { slideCount } = this.thumbnails.getInfo();
    const { index } = this.slider.getInfo();
    const indexToGoTo = evt.index > slideCount ? 0 : evt.index;

    if (indexToGoTo !== index) {
      this.slider.goTo( indexToGoTo );
    }
  }
}

new Slider();

// import { tns } from 'tiny-slider/src/tiny-slider';
//
// const tabActive = document.querySelector('.js-Tab__active');
// const tabs = document.querySelector('.js-Tabs');
//
// let oldLeft = 0;
//
// const slider = tns({
//   container: '.js-Tabs__slider',
//   mode: 'carousel',
//   items: 1,
//   loop: false,
//   center: true,
//   controls: false,
//   nav: true,
//   navPosition: 'bottom',
//   mouseDrag: true,
//   navAsThumbnails: true
// });
//
// const sTabs = tns({
//   container: '.js-Tabs',
//   mode: 'carousel',
//   items: 3,
//   loop: false,
//   center: false,
//   controls: false,
//   nav: true,
//   navPosition: 'bottom',
//   mouseDrag: true,
//   navAsThumbnails: true
// });
//
// slider.events.on('indexChanged', slider => {
//   let tab = tabs.children[slider.index].closest('.js-Tab');
//
//   if (tab) {
//     if (tabActive) {
//       const { width } = tab.getBoundingClientRect();
//       const { width: tabsWidth } = tabs.getBoundingClientRect();
//       const left = tab.offsetLeft + width / 2 - 75;
//
//       if (oldLeft < left) {
//         tabActive.style['-webkit-transition'] = 'left 100ms ease-in, right 100ms ease-out';
//         tabActive.style['transition'] = 'left 100ms ease-in, right 100ms ease-out';
//       } else {
//         tabActive.style['-webkit-transition'] = 'left 100ms ease-in, right 100ms ease-out';
//         tabActive.style['transition'] = 'left 100ms ease-out, right 100ms ease-in';
//       }
//
//       tabActive.style.left = `${left}px`;
//       tabActive.style.right = `${tabsWidth - width / 2 - 75 - tab.offsetLeft}px`;
//
//       oldLeft = left;
//     }
//   }
// });
//
// const onTab = e => {
//   let tab = e.target.closest('.js-Tab');
//
//   if (tab) {
//     if (tabActive) {
//       const { width } = tab.getBoundingClientRect();
//       const { width: tabsWidth } = tabs.getBoundingClientRect();
//       const left = tab.offsetLeft + width / 2 - 75;
//
//       if (oldLeft < left) {
//         tabActive.style['-webkit-transition'] = 'left 100ms ease-in, right 100ms ease-out';
//         tabActive.style['transition'] = 'left 100ms ease-in, right 100ms ease-out';
//       } else {
//         tabActive.style['-webkit-transition'] = 'left 100ms ease-in, right 100ms ease-out';
//         tabActive.style['transition'] = 'left 100ms ease-out, right 100ms ease-in';
//       }
//
//       tabActive.style.left = `${left}px`;
//       tabActive.style.right = `${tabsWidth - width / 2 - 75 - tab.offsetLeft}px`;
//
//       oldLeft = left;
//     }
//
//     if (slider) {
//       let i = 0;
//
//       while( tab.previousSibling != null ) {
//         i++;
//
//         tab = tab.previousSibling;
//       };
//
//       slider.goTo((i - 1) / 2);
//     }
//   }
// };
//
// const onLeft = e => {
//   const left = e.target.closest('.js-Tabs__arrow--left');
//
//   if (left && slider) {
//     slider.goTo('prev');
//   }
// };
//
// const onRight = e => {
//   const left = e.target.closest('.js-Tabs__arrow--right');
//
//   if (left && slider) {
//     slider.goTo('next');
//   }
// };
//
// addEventListener('click', onTab, false);
// addEventListener('click', onLeft, false);
// addEventListener('click', onRight, false);
// onTab({ target: document.querySelector('.js-Tab') });
