// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from '@/App';
import router from '@/router';
import store from '@/store';
import VueFullPage from 'vue-fullpage.js';
import 'fullpage.js/vendors/scrolloverflow';
import 'fullpage-vue/src/fullpage.css';
import VueFull from 'fullpage-vue';
import 'animate.css';
import Meta from 'vue-meta'; 

import Datetime from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';
Vue.use(Datetime);

import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard);

import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use( CKEditor );

import VueBulmaAccordion from 'vue-bulma-accordion';
Vue.use(VueBulmaAccordion);
import VueAwesomeSwiper from 'vue-awesome-swiper';
const VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo);

import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

// require styles
import 'swiper/dist/css/swiper.css';
Vue.use(VueAwesomeSwiper, /* { default global options } */)

// Vue.config.productionTip = false;
// Vue.use(Vuelidate);

import VueTimeago from 'vue-timeago';

Vue.use(VueTimeago, {
  name: 'Timeago',
  locale: 'en',
  locales: {
    'zh-CN': require('date-fns/locale/zh_cn'),
    ja: require('date-fns/locale/ja')
  }
})

Vue.use(VueFull);
Vue.use(VueFullPage);

var VueCookie = require('vue-cookie');
Vue.use(VueCookie);
 
Vue.use(router);
Vue.use(Meta);

import 'vue-multiselect/dist/vue-multiselect.min.css';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    hideMenu: false,
    openShopMenu: false,
    openCartMenu: {
      background: false,
      menu: false,
    },
    cartCount: 0
  },
  store,
  router,
  components: {
    App
  },
  template: '<App/>',
  render: h => h(App)
});


