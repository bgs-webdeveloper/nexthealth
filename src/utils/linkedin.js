import { helpers } from 'vuelidate/lib/validators';

export const linkedInRegex = /^(?:http(s)?:\/\/)?[\w.-]*linkedin\.com\/in\/([\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,;=.]+)$/;
export const linkedinValidator = helpers.regex('linkedin', linkedInRegex);