import Router from 'vue-router';
import Vue from 'vue';
import VueAnalytics from 'vue-analytics'

Vue.use(Router);

import NotFound from '@/components/not-found/NotFound';

import About from '@/components/main/about/About';
import Contact from '@/components/main/contact/Contact';
import Thanks from '@/components/main/contact/Thanks';
import ExecutivePhysical from '@/components/main/executive-physical/ExecutivePhysical';
import PersonalizedProgramsAndTreatments
from '@/components/main/personalized-programs-and-treatments/PersonalizedProgramsAndTreatments';
import PreventativeCare from '@/components/main/preventative-care/PreventativeCare';
import Services from '@/components/main/services/Services';
import TermsOfUse from '@/components/main/terms-of-use/TermsOfUse';
import Gold from '@/components/main/modals/Gold';
import GoldMobile from '@/components/main/modals/mobile/GoldMobile';
import PlatinumMobile from '@/components/main/modals/mobile/PlatinumMobile';
import SilverMobile from '@/components/main/modals/mobile/SilverMobile';
import Silver from '@/components/main/modals/Silver';
import HeartHealth from '@/components/main/modals/HeartHealth';
import Platinum from '@/components/main/modals/Platinum';
import PeptidesDetail from '@/components/main/peptides/Detail';
import PeptidesCategory from '@/components/main/peptides/Category';
import PeptidesCategories from '@/components/main/peptides/Categories';
import Homepage from '@/components/main/HomePage/HomePage';
import AdminCategory from '@/components/main/admin/category/AdminCategory';
import AdminLogin from '@/components/main/admin/login/AdminLogin';
import AdminResetPassword from '@/components/main/admin/login/AdminResetPassword';
import AdminLayout from '@/components/main/admin/AdminLayout';
import AdminUsers from '@/components/main/admin/users/AdminUsers';
import AdminProducts from '@/components/main/admin/products/AdminProducts'
import AdminAddProd from '@/components/main/admin/products/AdminAddProd'
import AdminEditProd from '@/components/main/admin/products/AdminEditProd'
import Order from '@/components/main/admin/order/AdminOrder';
import Subscriptions from '@/components/main/admin/subscriptions/AdminSubscriptions';
import AdminCoupons from '@/components/main/admin/coupons/AdminCoupons';
import AdminOrderInfo from '@/components/main/admin/order/AdminOrderInfo';
import ReviewsList from '@/components/main/admin/reviews/ReviewsList';
import Sitemap from '@/components/main/sitemap/Sitemap';

// Shop pages
import ShopLayout from '@/components/main/shop/ShopLayout';
import Product from '@/components/main/shop/product/product';
import Category from '@/components/main/shop/category/category';
import Home from '@/components/main/shop/home/home';
import ShopCart from '@/components/main/shop/cart/shopCart';
import UserLogin from '@/components/main/shop/login/userLogin';
import NewAccount from '@/components/main/shop/login/newAccount';
import PasswordRecovery from '@/components/main/shop/login/passwordRecovery';
import newPassword from '@/components/main/shop/login/newPassword';
// import myaccount from '@/components/main/shop/myaccount/myaccount';
import AccountLayout from '@/components/main/shop/myaccount/AccountLayout';
import Orders from '@/components/main/shop/myaccount/orders';
import subscription from '@/components/main/shop/myaccount/subscription';
import shippingAddress from '@/components/main/shop/myaccount/shippingAddress';
import profile from '@/components/main/shop/myaccount/profile';
import rewards from '@/components/main/shop/myaccount/Rewards';
import Checkout from '@/components/main/shop/checkout/checkout';
import VitaminsPage from '@/components/main/vitamins-page/VitaminsPage';

import MainLayout from '@/components/main/MainLayout';

import shopRedirectGuard from './Guards/shopRedirectGuard';
import AdminServices from "../components/main/admin/services/AdminServices";
import AdminAddService from "../components/main/admin/services/AdminAddService";
import AdminEditService from "../components/main/admin/services/AdminEditService";
import Service from "../components/main/service/Service";
import Landing from "../components/main/landing/landing";

const router = new Router({
  mode: 'history',
  routes: [{
      path: '',
      component: MainLayout,
      children: [{
          path: '/',
          component: Homepage,
          name: 'Homepage',
          meta: {
            title: 'Home'
          }
        },
        {
          path: '/schedule-appointment',
          component: Landing,
          name: 'Landing',
          meta: {
             title: 'Landing'
          }
        },
        {
          path: '/service/:slug',
          component: Service,
          name: 'Service',
          meta: {
            title: 'Service'
          }
        },
        {
          path: '/about',
          component: About,
          name: 'About',
          meta: {
            title: 'About'
          }
        },
        {
          path: '/platinum',
          component: Platinum,
          name: 'Platinum',
          meta: {
            title: 'Platinum'
          }
        },
        {
          path: '/HeartHealth',
          component: HeartHealth,
          name: 'HeartHealth',
          meta: {
            title: 'HeartHealth'
          }
        },
        {
          path: '/gold',
          component: Gold,
          name: 'Gold',
          meta: {
            title: 'Gold'
          }
        },
        {
          path: '/silvermobile',
          component: SilverMobile,
          name: 'SilverMobile',
          meta: {
            title: 'SilverMobile'
          }
        },

        {
          path: '/goldMobile',
          component: GoldMobile,
          name: 'GoldMobile',
          meta: {
            title: 'GoldMobile'
          }
        },
        {
          path: '/PlatinumMobile',
          component: PlatinumMobile,
          name: 'PlatinumMobile',
          meta: {
            title: 'PlatinumMobile'
          }
        },
        {
          path: '/silver',
          component: Silver,
          name: 'Silver',
          meta: {
            title: 'Silver'
          }
        },
        {
          path: '/contact',
          component: Contact,
          name: 'Contact',
          meta: {
            title: 'Contact'
          }
        },
        {
            path: '/contact/thanks',
            component: Thanks,
            name: 'Thanks',
            meta: {
                title: 'Thank you'
            }
        },
        {
          path: '/executive-physical',
          component: ExecutivePhysical,
          name: 'Executive Physical',
          meta: {
            title: 'Executive Physical'
          }
        },
        {
          path: '/personalized-programs-and-treatments',
          component: PersonalizedProgramsAndTreatments,
          name: 'Personalized Programs And Treatments',
          meta: {
            title: 'Personalized Programs And Treatments'
          }
        },
        {
          path: '/preventative-care',
          component: PreventativeCare,
          name: 'Preventative Care',
          meta: {
            title: 'Preventative Care'
          }
        },
        {
          path: '/services',
          component: Services,
          name: 'Services',
          meta: {
            title: 'Services'
          }
        },
        {
          path: '/iv-vitamin-therapy',
          component: VitaminsPage,
          name: "VitaminsPage",
          meta: {
            title: "IV Vitamins"
          }
        },
        {
          path: '/terms-of-use',
          component: TermsOfUse,
          name: 'Terms Of Use',
          meta: {
            title: 'Terms Of Use'
          }
        },
        {
          path: '/peptides',
          component: PeptidesCategories,
          name: 'Peptides Categories',
          meta: {
            title: 'Peptides Categories'
          }
        },
        {
          path: '/peptides/category/:id',
          component: PeptidesCategory,
          name: 'Peptides Category',
          meta: {
            title: 'Peptides Category'
          }
        },
        {
          path: '/peptides/detail/:id',
          component: PeptidesDetail,
          name: 'Peptides Detail',
          meta: {
            title: 'Peptides Detail'
          }
        },
        {
          path: '/password_reset',
          component: newPassword,
          name: 'Reset Password',
          meta: {
            title: 'Reset Password'
          }
        },
        {
          path: '/sitemap',
          component: Sitemap,
          name: 'Sitemap',
          meta: {
            title: 'Sitemap'
          }
        }
      ]
    },
    {
      path: '/admin',
      component: AdminLayout,
      redirect: 'admin/products',
      children: [{
          path: 'login',
          component: AdminLogin,
          name: 'AdminLogin',
          meta: {
            guest: true,
            title: 'Login'
          }
        },
        {
          path: 'coupons',
          component: AdminCoupons,
          name: 'AdminCoupons',
          meta: {
            guest: false,
            title: 'Coupons'
          }
        },
        {
          path: 'categories',
          component: AdminCategory,
          name: 'AdminCategory',
          meta: {
            guest: false,
            title: 'Categories'
          }
        },
        {
          path: 'orders',
          component: Order,
          name: 'Order',
          meta: {
            guest: false,
            title: 'Order list'
          }
        },
        {
          path: 'order/:id',
          component: AdminOrderInfo,
          name: 'AdminOrderInfo',
          meta: {
            guest: false,
            title: 'Order Info'
          }
        },
        {
          path: 'subscriptions',
          component: Subscriptions,
          name: 'Subscriptions',
          meta: {
            guest: false,
            title: 'Subscriptions'
          }
        },
        {
          path: 'password_reset',
          component: AdminResetPassword,
          name: 'AdminResetPassword',
          meta: {
            guest: true,
            title: 'Reset Password'
          }
        },
        {
          path: 'users',
          component: AdminUsers,
          name: 'AdminUsers',
          meta: {
            guest: false,
            title: 'Customers'

          }
        },
        {
          path: 'products',
          component: AdminProducts,
          name: 'AdminProducts',
          meta: {
            guest: false,
            title: 'Product list'
          }
        },
        {
          path: 'product/add',
          component: AdminAddProd,
          name: 'AdminAddProd',
          meta: {
            guest: false,
            title: 'Add product'
          }
        },
        {
          path: 'product/:id',
          component: AdminEditProd,
          name: 'AdminEditProd',
          meta: {
            guest: false,
            title: 'Edit product'
          }
        },
          {
              path: 'services',
              component: AdminServices,
              name: 'AdminServices',
              meta: {
                  guest: false,
                  title: 'Service list'
              }
          },
          {
              path: 'service/add',
              component: AdminAddService,
              name: 'AdminAddService',
              meta: {
                  guest: false,
                  title: 'Add service'
              }
          },
          {
              path: 'service/:id',
              component: AdminEditService,
              name: 'AdminEditService',
              meta: {
                  guest: false,
                  title: 'Edit service'
              }
          },
        {
          path: 'reviews-list',
          component: ReviewsList,
          name: 'ReviewsList',
          meta: {
            guest: false,
            title: 'Reviews list'
          }
        },
      ]
    },

    // shop pages
    {
      path: '/shop',
      component: ShopLayout,
      beforeEnter: shopRedirectGuard,
      children: [{
          path: '/',
          component: Home,
          name: 'Home',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'Shop'
          }
        },
        {
          path: 'login',
          component: UserLogin,
          name: 'UserLogin',
          meta: {
            guest: true,
            onlyGuest: true,
            title: 'Login'
          }
        },
        {
          path: 'new-account',
          component: NewAccount,
          name: 'newAccount',
          meta: {
            guest: true,
            onlyGuest: true,
            title: 'Sign up'
          }
        },
        {
          path: 'password_reset',
          component: PasswordRecovery,
          name: 'PasswordRecovery',
          meta: {
            guest: true,
            onlyGuest: true,
            title: 'Password Recovery'
          }
        },
        {
          path: 'cart',
          component: ShopCart,
          name: 'ShopCart',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'Cart'
          }
        },
        {
          path: 'checkout',
          component: Checkout,
          name: 'checkout',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'Checkout'
          }
        },
        {
          path: 'thank-you',
          component: Checkout,
          name: 'thank-you',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'Checkout'
          }
        },
        {
          path: 'account',
          component: AccountLayout,
          // name: 'account',
          children: [{
              path: 'orders',
              component: Orders,
              name: 'orders',
              meta: {
                guest: false,
                onlyGuest: false,
                title: 'Order History'
              }
            },
            {
              path: 'subscription',
              component: subscription,
              name: 'subscription',
              meta: {
                guest: false,
                onlyGuest: false,
                title: 'Subscription orders'
              }
            },
            {
              path: 'shipping',
              component: shippingAddress,
              name: 'shippingAddress',
              meta: {
                guest: false,
                onlyGuest: false,
                title: 'Addresses'
              }
            },
            {
              path: 'rewards',
              component: rewards,
              name: 'rewards',
              meta: {
                guest: false,
                onlyGuest: false,
                title: 'Rewards'
              }
            },
            {
              path: 'profile',
              component: profile,
              name: 'profile',
              meta: {
                guest: false,
                onlyGuest: false,
                title: 'Edit Profile'
              }
            },
          ],
        },
        {
          path: 'product/:slug',
          component: Product,
          name: 'Product',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'Product'
          }
        },
        {
          path: 'category/all',
          component: Category,
          name: 'all_products',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'All products'
          }
        },
        {
          path: 'category/:slug',
          component: Category,
          name: 'Category',
          meta: {
            guest: true,
            onlyGuest: false,
            title: 'Category'
          }
        },
      ]
    },
    {
      path: '*',
      component: MainLayout,
      children: [{
        path: '',
        component: NotFound,
        name: 'NotFound'
      }]
    }
  ]
});

Vue.use(VueAnalytics, {
  id: 'UA-125430344-1',
  router
});

export default router;
